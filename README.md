SignUtilities
=============

A Bukkit plugin providing the abilities to edit signs, copy and paste text, add colors, display links, run commands, activate redstone, drop minecarts, etc.

http://dev.bukkit.org/server-mods/signutilities/

This plugin was forked with permission from the original SignUtilities by timcolonel.

License
=======

See LICENSE.txt