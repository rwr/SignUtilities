/**
 * SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 * Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.timcolonel.SignUtilities;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import com.timcolonel.SignUtilities.SignUtilities;
import java.util.Set;
import org.bukkit.World;

public class Utils {

	public static Boolean isASign(Block block) {
		if (block.getType() == Material.SIGN_POST || block.getType() == Material.WALL_SIGN) {
			return true;
		} else {
			return false;
		}
	}

	public static Boolean isAnActivator(Block block) {
		/*
		 * If the block is either a -button -plate(stone,wood) -lever
		 */
		if (block.getType() == Material.STONE_BUTTON || block.getType() == Material.WOOD_BUTTON || block.getType() == Material.STONE_PLATE || block.getType() == Material.WOOD_PLATE || block.getType() == Material.LEVER) {
			return true;
		} else {
			return false;
		}
	}

	public static int containsCmdLinks(String[] lines) {
		int i = 0;
		for (String line : lines) {
			if (line.contains("[") && line.contains("]")) {
				String str;
				str = Colors.removeColor(line);
				str = str.substring(1, str.length() - 1);

				if (SignUtilities.instance.config.cmdLinks.containsKey(str)) {
					return i;
				}
			}

			i++;
		}

		// Not found
		return -1;
	}

	/**
	 * Return the first line that is a weblink
	 *
	 * @param lines
	 * @return
	 */
	public static int containsWebLinks(String[] lines) {
		int i = 0;
		for (String line : lines) {
			if (line.contains("(") && line.contains(")")) {
				String str;
				str = Colors.removeColor(line);
				str = str.substring(1, str.length() - 1);

				if (SignUtilities.instance.config.webLinks.containsKey(str)) {
					return i;
				}
			}

			i++;
		}

		// Not found
		return -1;
	}

	public static Boolean isActivate(Block block, Action action) {
		/*
		 * If the block is either a -button -plate(stone,wood) -lever
		 */
		if ((block.getType() == Material.LEVER || block.getType() == Material.STONE_BUTTON || block.getType() == Material.WOOD_BUTTON) && (action == Action.LEFT_CLICK_BLOCK || action == Action.RIGHT_CLICK_BLOCK)) {
			return true;
		} else if ((block.getType() == Material.WOOD_PLATE || block.getType() == Material.STONE_PLATE) && action == Action.PHYSICAL) {
			return true;
		} else {
			return false;
		}
	}

	public static String blockToString(Block b) {
		return "(" + b.getX() + "," + b.getY() + "," + b.getZ() + "," + b.getWorld().getName() + ")";
	}

	/**
	 * Check if the player can edit the block
	 *
	 * @param playerName
	 * @param block
	 * @return
	 */
	public static boolean canEdit(String playerName, Block block) {
		// block place and break simulation method

		Player player = SignUtilities.instance.getServer().getPlayer(playerName);
		
		// can't edit a block if there is no block
		if(block == null) {
			return false;
		}
		
		Location loc = block.getLocation();

		// only allow them to select a sign if they can place and break there
		if (checkBlockBreakSimulation(loc, player) && checkBlockPlaceSimulation(loc, block.getTypeId(), block.getData(), loc, player)) {
			return true;
		} else {
			return false;
		}

	}

	/* Lyneira's Code Start */
	public static boolean checkBlockPlaceSimulation(Location target, int typeId, byte data, Location placedAgainst, Player player) {
		Block placedBlock = target.getBlock();
		BlockState replacedBlockState = placedBlock.getState();
		int oldType = replacedBlockState.getTypeId();
		byte oldData = replacedBlockState.getRawData();

		// save simulation location
		SignUtilities.instance.addSimLocation(target);

		// Set the new state without physics.
		placedBlock.setTypeIdAndData(typeId, data, false);
		BlockPlaceEvent placeEvent = new BlockPlaceEvent(placedBlock, replacedBlockState, placedAgainst.getBlock(), null, player, true);
		SignUtilities.instance.getServer().getPluginManager().callEvent(placeEvent);

		// Revert to the old state without physics.
		placedBlock.setTypeIdAndData(oldType, oldData, false);

		// check if the simulation location is still available or not
		if (SignUtilities.instance.hasSimLocation(target)) {
			SignUtilities.instance.remSimLocation(target);
			return false;
		} else {
			return true;
		}
	}

	public static boolean checkBlockBreakSimulation(Location target, Player player) {
		// save simulation location
		SignUtilities.instance.addSimLocation(target);

		Block block = target.getBlock();
		BlockBreakEvent breakEvent = new BlockBreakEvent(block, player);
		SignUtilities.instance.getServer().getPluginManager().callEvent(breakEvent);

		// check if the simulation location is still available or not
		if (SignUtilities.instance.hasSimLocation(target)) {
			SignUtilities.instance.remSimLocation(target);
			return false;
		} else {
			return true;
		}
	}
	/* Lyneira's Code End */

	/**
	 * Finds the closest block in a vertical column.
	 * http://forums.bukkit.org/threads/get-nearest-wood-block.103091/
	 *
	 * @param origin The location around which to search.
	 * This location will NOT be included in the search, but all other locations in the column will.
	 * @param types A Set (preferably a HashSet) that contains the type IDs of blocks to search for
	 * @return The closest block, or null if one was not found in the column.
	 * In the case of a tie, the higher block wins.
	 */
	public static Block closestBlock(Location origin, Set<Integer> types) {
		int x = origin.getBlockX();
		int y = origin.getBlockY();
		int z = origin.getBlockZ();
		World world = origin.getWorld();

		for (int cy = 512; cy > 2; cy--) {
			int testY;
			if ((cy & 1) == 0) //if even
			{
				testY = y + cy / 2;
				if (testY > 255) {
					continue;
				}
			} else {
				testY = y - cy / 2;
				if (testY < 0) {
					continue;
				}
			}

			if (types.contains(world.getBlockTypeIdAt(x, testY, z))) {
				return world.getBlockAt(x, testY, z);
			}
		}

		return null;
	}
}
