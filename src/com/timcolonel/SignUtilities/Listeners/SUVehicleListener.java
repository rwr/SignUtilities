/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Listeners;

import org.bukkit.entity.Minecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;

import com.timcolonel.SignUtilities.SignUtilities;
import org.bukkit.entity.Boat;

//extends VehicleListener
public class SUVehicleListener implements Listener {

	@EventHandler
	public void onVehicleDestroy(VehicleDestroyEvent event) {
		try {
			Minecart m = (Minecart) event.getVehicle();
			if (SignUtilities.instance.minecartMgr.minecarts.containsKey(m)) {

				event.setCancelled(true);
				SignUtilities.instance.minecartMgr.minecartDestroyed(m);
			}
		} catch (Exception e) {

		}
		try {
			Boat b = (Boat) event.getVehicle();
			if (SignUtilities.instance.boatMgr.boats.containsKey(b)) {

				event.setCancelled(true);
				SignUtilities.instance.boatMgr.boatDestroyed(b);
			}
		} catch (Exception e) {

		}

	}

	@EventHandler
	public void onVehicleEnter(VehicleEnterEvent event) {

	}

	@EventHandler
	public void onVehicleDamage(VehicleDamageEvent event) {
		try {
			Minecart m = (Minecart) event.getVehicle();
			if (SignUtilities.instance.minecartMgr.minecarts.containsKey(m)) {
				m.setVelocity(event.getAttacker().getLocation().getDirection());
			}
		} catch (Exception e) {

		}

		try {
			Boat b = (Boat) event.getVehicle();
			if (SignUtilities.instance.boatMgr.boats.containsKey(b)) {
				b.setVelocity(event.getAttacker().getLocation().getDirection());
			}
		} catch (Exception e) {

		}

	}

}
