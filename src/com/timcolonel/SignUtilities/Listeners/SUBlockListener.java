/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.inventory.ItemStack;

import com.timcolonel.SignUtilities.Colors;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.SignUtilitiesClipBoard;
import com.timcolonel.SignUtilities.Utils;

//extends BlockListener
public class SUBlockListener implements Listener {

	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		Player player = event.getPlayer();

		// Check if the user add colors
		for (int i = 0; i < 4; i++) {
			String line = event.getLine(i);
			if (SignUtilities.instance.pluginsMgr.canColorSign(player))
				event.setLine(i, Colors.findColor(line));
			else
				event.setLine(i, line);
		}

		String firstLine = event.getLine(0);

		if (firstLine.contains("info:")) {
			if (SignUtilities.instance.pluginsMgr.hasPermission(player, "signutilities.signs.news.create")) {
				event.setLine(0, ChatColor.DARK_BLUE + firstLine);
			} else {
				// Destroy the sign
				event.getBlock().setTypeId(0);
				event.getPlayer().setItemInHand(new ItemStack(323, 1));
				return;
			}

		} else if (firstLine.contains("[cmd]")) {
			if (SignUtilities.instance.pluginsMgr.hasPermission(event.getPlayer(), "signutilities.signs.cmd.create")) {
				for (int i = 0; i < 4; i++) {
					String color = SignUtilities.instance.config.cmdSignColor[i];
					if (color.equals("") && i == 0) {
						event.setLine(0, ChatColor.DARK_BLUE + firstLine);
					}
					if (color.length() == 2 && color.charAt(0) == '&') {
						event.setLine(i, color + event.getLine(i));
					}

				}
			} else {
				// Destroy the sign
				event.getBlock().setTypeId(0);
				event.getPlayer().setItemInHand(new ItemStack(323, 1));
				return;
			}

		} else if (firstLine.contains("[minecart]") || firstLine.contains("[minecart:rem]")) {
			if (SignUtilities.instance.pluginsMgr.hasPermission(event.getPlayer(), "signutilities.signs.minecart.create")) {
				event.setLine(0, ChatColor.DARK_BLUE + firstLine);
			} else {
				// Destroy the sign
				event.getBlock().setTypeId(0);
				event.getPlayer().setItemInHand(new ItemStack(323, 1));
				return;
			}

		} else if (firstLine.contains("[boat]") || firstLine.contains("[boat:rem]")) {
			if (SignUtilities.instance.pluginsMgr.hasPermission(event.getPlayer(), "signutilities.signs.boat.create")) {
				event.setLine(0, ChatColor.DARK_BLUE + firstLine);
			} else {
				// Destroy the sign
				event.getBlock().setTypeId(0);
				event.getPlayer().setItemInHand(new ItemStack(323, 1));
				return;
			}

		} else if (firstLine.contains("[activator]")) {
			if (SignUtilities.instance.pluginsMgr.hasPermission(event.getPlayer(), "signutilities.signs.activator.create")) {
				event.setLine(0, ChatColor.DARK_BLUE + firstLine);
			} else {
				// Destroy the sign
				event.getBlock().setTypeId(0);
				event.getPlayer().setItemInHand(new ItemStack(323, 1));
				return;
			}
		}

		/*
		 * else if(firstLine.contains("[") && firstLine.contains("]")) //CUSTOM CMD SIGNS { int i =0; for(String line: event.getLines()) { String str = Colors.removeColor(line); String key =
		 * str.substring(1, str.length() -1); //Custom cmd link found if(plugin.config.cmdLinks.containsKey(key)) { if(plugin.pluginsMgr.hasPermission(event.getPlayer(),
		 * "signutils.cmd.custom.create")) { event.setLine(i, ChatColor.DARK_BLUE + firstLine); break; } else //Destroy the sign { event.getBlock().setTypeId(0); event.getPlayer().setItemInHand(new
		 * ItemStack(323,1)); return; } } i++; } } else if(firstLine.contains("(") && firstLine.contains(")")) //CUSTOM CMD SIGNS { int i =0; for(String line: event.getLines()) { String str =
		 * Colors.removeColor(line); String key = str.substring(1, str.length() -1); //Custom cmd link found if(plugin.config.cmdLinks.containsKey(key)) {
		 * if(plugin.pluginsMgr.hasPermission(event.getPlayer(), "signutils.cmd.custom.create")) { event.setLine(i, ChatColor.DARK_BLUE + firstLine); break; } else //Destroy the sign {
		 * event.getBlock().setTypeId(0); event.getPlayer().setItemInHand(new ItemStack(323,1)); return; } } i++; } }
		 */

		int result = Utils.containsCmdLinks(event.getLines());

		if (result != -1) {
			if (SignUtilities.instance.pluginsMgr.hasPermission(event.getPlayer(), "signutilities.signs.cmd.custom.create")) {
				event.setLine(result, ChatColor.DARK_BLUE + event.getLine(result));
			} else {
				// Destroy the sign
				event.getBlock().setTypeId(0);
				event.getPlayer().setItemInHand(new ItemStack(323, 1));
				return;
			}
		}

		result = Utils.containsWebLinks(event.getLines());
		if (result != -1) {
			if (SignUtilities.instance.pluginsMgr.hasPermission(event.getPlayer(), "signutilities.signs.web.create")) {
				event.setLine(result, ChatColor.DARK_BLUE + event.getLine(result));
			} else {
				// Destroy the sign
				event.getBlock().setTypeId(0);
				event.getPlayer().setItemInHand(new ItemStack(323, 1));
				return;
			}
		}

		// make sure the block is a sign and get it's state
		BlockState signBlockState = event.getBlock().getState();
		Sign sign;
		if (signBlockState instanceof Sign) {
			sign = (Sign) signBlockState;
		} else {
			return;
		}

		String str = sign.getX() + ";" + sign.getY() + ";" + sign.getZ();
		if (SignUtilities.instance.signMgr.tempSigns.containsKey(str)) {

			// Put all the content of the temp sign in the sign to be edited
			Sign editSign = (Sign) SignUtilities.instance.signMgr.tempSigns.get(str).getState();
			String[] lines = event.getLines();

			for (int i = 0; i < 4; i++) {
				editSign.setLine(i, lines[i]);
			}

			editSign.update();

			// Delete the temp Sign
			event.getBlock().setTypeId(0);
			SignUtilities.instance.signMgr.tempSigns.remove(str);
			event.getPlayer().setItemInHand(new ItemStack(323, 1));
		}

	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if (!SignUtilities.instance.config.keepSignCopy) {
			return;
		}

		Player player = event.getPlayer();

		// Paste the player clipboard if one exists
		if (SignUtilities.instance.playerClipBoard.containsKey(player)) {

			SignUtilitiesClipBoard cb = SignUtilities.instance.playerClipBoard.get(player);

			// make sure the block is a sign and get it's state
			BlockState signBlockState = event.getBlock().getState();
			Sign sign;
			if (signBlockState instanceof Sign) {
				sign = (Sign) signBlockState;
			} else {
				return;
			}

			if (!cb.paste) {
				for (int i = 0; i < cb.lines.size(); i++) {
					int lineNb = cb.lines.get(i);
					String line = Colors.decodeColor(cb.content.get(i));
					sign.setLine(lineNb, line);

				}

				if (!SignUtilities.instance.config.keepSignCopy) {
					SignUtilities.instance.playerClipBoard.get(player).paste = true;
				}

			}
		}
	}

	/**
	 * Check for SignUtilities simulation BlockPlaceEvent
	 * 
	 * We do this to prevent other plugins from picking up the event.
	 * 
	 * @param event
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void checkBlockPlaceSim(BlockPlaceEvent event) {
		Location l = event.getBlock().getLocation();
		if (SignUtilities.instance.hasSimLocation(l)) {
			if (!event.isCancelled()) {
				event.setCancelled(true);
			}
			SignUtilities.instance.remSimLocation(l);
		}
	}

	/**
	 * Check for SignUtilities simulation BlockBreakEvent
	 * 
	 * We do this to prevent other plugins from picking up the event.
	 * 
	 * @param event
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void checkBlockBreakSim(BlockBreakEvent event) {
		Location l = event.getBlock().getLocation();
		if (SignUtilities.instance.hasSimLocation(l)) {
			if (!event.isCancelled()) {
				event.setCancelled(true);
			}
			SignUtilities.instance.remSimLocation(l);
		}
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {

		Player p = event.getPlayer();

		if (p.getItemInHand().getTypeId() == SignUtilities.instance.config.itemSel) {
			event.setCancelled(true);
		}
	}

	public void onBlockRedstoneChange(BlockRedstoneEvent event) {

	}

}