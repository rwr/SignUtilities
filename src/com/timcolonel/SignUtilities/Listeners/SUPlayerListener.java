/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Listeners;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Button;

import com.timcolonel.SignUtilities.SignAction;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;

//extends PlayerListener
public class SUPlayerListener implements Listener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		// If the interact is a block
		if (event.hasBlock()) {
			Player p = event.getPlayer();
			boolean cancelEvent = false;

			// ////////////////////////////////////////////////////////////
			// WIRELESS SELECTION //
			// ////////////////////////////////////////////////////////////

			if (p.getItemInHand().getTypeId() == SignUtilities.instance.config.itemSel) {
				if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
					if (SignUtilities.instance.config.dispMsg >= 0) {
						p.sendMessage(ChatColor.GREEN + "First block selected");
					}
					SignUtilities.instance.linkMgr.setPointA(p, event.getClickedBlock().getLocation());
					cancelEvent = true;

				} else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (SignUtilities.instance.config.dispMsg >= 0) {
						p.sendMessage(ChatColor.GREEN + "Second block selected");
					}
					SignUtilities.instance.linkMgr.setPointB(p, event.getClickedBlock().getLocation());
					cancelEvent = true;
				}
			}

			// ////////////////////////////////////////////////////////////
			// ACTIVATOR //
			// ////////////////////////////////////////////////////////////
			Block clickedBlock = event.getClickedBlock();
			// Create the block that will be activated
			Block blockToActivate = event.getClickedBlock();
			if (!cancelEvent && clickedBlock.getType() == Material.STONE_PLATE && event.getAction() == Action.PHYSICAL) {
				blockToActivate = clickedBlock.getRelative(0, -2, 0);
			}

			if (!cancelEvent && clickedBlock.getType() == Material.STONE_BUTTON && (event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK)) {

				byte data = clickedBlock.getData();
				Button btn = new Button(clickedBlock.getType(), data);

				BlockFace face = btn.getAttachedFace();
				if (face != null) {
					blockToActivate = clickedBlock.getRelative(face, 2);
				}
			}

			String str = Utils.blockToString(event.getClickedBlock());
			if (!cancelEvent && Utils.isActivate(event.getClickedBlock(), event.getAction()) && SignUtilities.instance.wlSignMgr.blockHasConnection(str)) {
				ArrayList<String> list = SignUtilities.instance.wlSignMgr.wirelessSigns.get(str);
				for (String signStr : list) {
					Block b = SignUtilities.instance.stringToBlock(signStr);
					if (b != null) {
						if (Utils.isASign(b)) {

							SignUtilities.instance.signMgr.runSignCommand(p, b, event.getClickedBlock(), event.getBlockFace(), SignAction.LeftClick);

						}
					}
				}
			}

			boolean showMsg = false;

			// Add the block to the player selection
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {

				// player isn't sneaking so just select the sign
				SignUtilities.instance.signSelMgr.addSelection(p, clickedBlock);
				showMsg = true;
			}

			// If the block is a sign
			if (!cancelEvent && Utils.isASign(blockToActivate)) {
				boolean signCmd = false;
				if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					signCmd = SignUtilities.instance.signMgr.runSignCommand(p, blockToActivate, event.getClickedBlock(), event.getBlockFace(), SignAction.RightClick);
				} else {
					signCmd = SignUtilities.instance.signMgr.runSignCommand(p, blockToActivate, event.getClickedBlock(), event.getBlockFace(), SignAction.LeftClick);
				}

				if (showMsg) {
					displayMessage(p, signCmd);
				}
			}

		}

	}

	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		try {
			Minecart m = (Minecart) event.getRightClicked();
			if (SignUtilities.instance.minecartMgr.minecarts.containsKey(m)) {
				m.setVelocity(event.getPlayer().getLocation().getDirection());
			}
		} catch (Exception e) {
		}

		try {
			Boat b = (Boat) event.getRightClicked();
			if (SignUtilities.instance.boatMgr.boats.containsKey(b)) {
				b.setVelocity(event.getPlayer().getLocation().getDirection());
			}
		} catch (Exception e) {
		}
	}

	private void displayMessage(Player player, boolean commandSign) {
		if (SignUtilities.instance.config.dispMsg >= 0) {
			if ((!commandSign || SignUtilities.instance.config.dispMsg == 2) && player.getItemInHand().getTypeId() != 323) {
				player.sendMessage(ChatColor.BLUE + "You selected a sign");

			}
		}
	}
}
