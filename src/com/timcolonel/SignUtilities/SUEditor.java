/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignUtilities;

public class SUEditor {

	public boolean changeText(Sign sign, String text, int line, Player p) {
		if (line == 0) {
			String[] lines = text.split(";", 4);

			int i = 0;

			for (String l : lines) {
				if (SignUtilities.instance.pluginsMgr.canColorSign(p))
					sign.setLine(i, Colors.findColor(l));
				else
					sign.setLine(i, l);

				i++;
			}

			// Clear the line left
			while (i < 4) {
				sign.setLine(i, "");
				i++;
			}
		} else {
			String ctext = Colors.findColor(text);
			sign.setLine(line, ctext);
		}

		sign.update();
		return true;
	}

}
