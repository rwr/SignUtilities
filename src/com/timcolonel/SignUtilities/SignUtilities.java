/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Location;
import org.bukkit.World;

import com.timcolonel.SignUtilities.CommandHandler.CommandManager;
import com.timcolonel.SignUtilities.ExternalPlugins.SUPluginsManager;
import com.timcolonel.SignUtilities.Files.MainConfig;
import com.timcolonel.SignUtilities.Listeners.SUBlockListener;
import com.timcolonel.SignUtilities.Listeners.SUPlayerListener;
import com.timcolonel.SignUtilities.Listeners.SUVehicleListener;
import com.timcolonel.SignUtilities.Manager.ColorsManager;
import com.timcolonel.SignUtilities.Manager.NewsManager;
import com.timcolonel.SignUtilities.Manager.PlayerLinkSelectionManager;
import com.timcolonel.SignUtilities.Manager.PlayerSignSelectionManager;
import com.timcolonel.SignUtilities.Manager.SUMinecartManager;
import com.timcolonel.SignUtilities.Manager.SUBoatManager;
import com.timcolonel.SignUtilities.Manager.SUSignManager;
import com.timcolonel.SignUtilities.Manager.WirelessSignManager;

public class SignUtilities extends JavaPlugin {

	public static SignUtilities instance;
	public static final Logger logger = Logger.getLogger("Minecraft");
	private List<Location> simLocations = new ArrayList<Location>();

	/*****************************************************************
	 * Creation of config files
	 ******************************************************************/

	public String enabledstartup = "Enabled On Startup";
	public boolean enabled;
	public MainConfig config;

	// listeners
	private SUBlockListener blockListener;
	private SUPlayerListener playerListener;
	private SUVehicleListener vehicleListener;

	// managers
	private CommandManager cmdManager;
	public SUPluginsManager pluginsMgr;
	public SUSignManager signMgr;
	public WirelessSignManager wlSignMgr;
	public SUMinecartManager minecartMgr;
	public SUBoatManager boatMgr;
	public NewsManager newsMgr;
	public ColorsManager colorsMgr;
	public PlayerSignSelectionManager signSelMgr;
	public PlayerLinkSelectionManager linkMgr;

	public HashMap<Player, SignUtilitiesClipBoard> playerClipBoard;

	@Override
	public void onEnable() {
		// SignUtilities
		instance = this;

		// listeners
		blockListener = new SUBlockListener();
		playerListener = new SUPlayerListener();
		vehicleListener = new SUVehicleListener();

		// managers
		cmdManager = new CommandManager();
		pluginsMgr = new SUPluginsManager();
		signMgr = new SUSignManager();
		wlSignMgr = new WirelessSignManager();
		minecartMgr = new SUMinecartManager();
		boatMgr = new SUBoatManager();
		newsMgr = new NewsManager();
		colorsMgr = new ColorsManager();
		signSelMgr = new PlayerSignSelectionManager();
		linkMgr = new PlayerLinkSelectionManager();

		playerClipBoard = new HashMap<Player, SignUtilitiesClipBoard>();

		config = new MainConfig("config.yml");
		config.loadConfig();

		colorsMgr.loadColor();
		wlSignMgr.loadWireLessSigns();
		if (!pluginsMgr.setupPlugins()) {
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		// //////////////////////////////////////////////////////////////////////////////////////
		// SET THE LISTENERS //
		// //////////////////////////////////////////////////////////////////////////////////////
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(blockListener, this);
		pm.registerEvents(playerListener, this);
		pm.registerEvents(vehicleListener, this);

	}

	@Override
	public void onDisable() {
		// save wireless signs
		wlSignMgr.saveWireLessSigns();

		// put any minecarts back in their chests
		minecartMgr.removeAllMinecarts();

		// put any boats back in their chests
		boatMgr.removeAllBoats();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		return cmdManager.manage(sender, cmd, commandLabel, args);
	}

	public Block stringToBlock(String str) {
		String[] v = str.substring(1, str.length() - 1).split(",");
		if (v.length == 4) {
			int x = 0;
			int y = 0;
			int z = 0;
			World w = null;

			try {
				x = Integer.parseInt(v[0]);
				y = Integer.parseInt(v[1]);
				z = Integer.parseInt(v[2]);
				w = this.getServer().getWorld(v[3]);
			} catch (Exception e) {
				logger.warning("The string is not in a block format.");
				return null;
			}

			Location l = new Location(w, x, y, z);
			return l.getBlock();
		} else {
			logger.warning("The string is not in a block format.");
			return null;
		}
	}

	public List<Location> getSimLocations() {
		return simLocations;
	}

	public void setSimLocations(List<Location> simLocations) {
		this.simLocations = simLocations;
	}

	public void addSimLocation(Location simLocation) {
		simLocations.add(simLocation);
	}

	public void remSimLocation(Location simLocation) {
		simLocations.remove(simLocation);
	}

	public boolean hasSimLocation(Location simLocation) {
		if (simLocations.contains(simLocation)) {
			return true;
		} else {
			return false;
		}
	}
}