/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities;

import org.bukkit.Location;
import org.bukkit.block.Block;

public class WirelessGroup {
	public Block activator;
	public Block sign;

	public WirelessGroup() {
		activator = null;
		sign = null;
	}

	public WirelessGroup(Block a, Block s) {
		activator = a;
		sign = s;
	}

	public WirelessGroup(Selection sel) {
		activator = null;
		sign = null;

		fromSelection(sel);
	}

	public boolean fromSelection(Selection sel) {
		Location l1 = sel.getPointA();
		Location l2 = sel.getPointB();

		Block b1 = l1.getBlock();
		Block b2 = l2.getBlock();

		if (Utils.isASign(b1) && Utils.isAnActivator(b2)) {
			sign = b1;
			activator = b2;
		} else if (Utils.isASign(b2) && Utils.isAnActivator(b1)) {
			sign = b2;
			activator = b1;
		} else {
			return false;
		}
		return true;

	}
}
