/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.SignUtilitiesClipBoard;
import com.timcolonel.SignUtilities.Utils;

public class CopyHandler extends CommandHandler {
	public CopyHandler(CommandSender sender, String[] args) {
		super(sender, args);
		permission = "signutilities.commands.scopy";
	}

	@Override
	public boolean execute() {
		// Permissions
		if (!hasPermission()) {
			return false;
		}

		if (SignUtilities.instance.signSelMgr.hasPlayerSelected((Player) sender)) {

			// Check if the sign is there
			if (Utils.isASign(SignUtilities.instance.signSelMgr.getSelection((Player) sender))) {
				Sign s = (Sign) SignUtilities.instance.signSelMgr.getSelection((Player) sender).getState();

				// Copy sign
				if (args.length == 0) {
					SignUtilitiesClipBoard cb = new SignUtilitiesClipBoard();
					for (int i = 0; i < 4; i++) {
						cb.content.add(s.getLine(i));
						cb.lines.add(i);
					}

					SignUtilities.instance.playerClipBoard.put((Player) sender, cb);
					sender.sendMessage(ChatColor.BLUE + "Sign copied");
					return true;

				} else if (args.length > 4) {
					// Means there are too many args
					sender.sendMessage(ChatColor.RED + "Too many arguments");
					return false;

				} else {
					// Copy only the specified lines
					SignUtilitiesClipBoard cb = new SignUtilitiesClipBoard();

					for (int i = 0; i < args.length; i++) {
						try {
							int l = Integer.parseInt(args[i]);
							l--;

							// Check if the line number is real
							if (l < 0 || l > 4) {
								sender.sendMessage(ChatColor.RED + "There is only four lines on a sign");
								return false;
							}

							cb.lines.add(l);
							cb.content.add(s.getLine(l));

						} catch (Exception e) {
							sender.sendMessage(ChatColor.RED + "Number Expected");
							return false;
						}
					}

					SignUtilities.instance.playerClipBoard.put((Player) sender, cb);
					sender.sendMessage(ChatColor.BLUE + "Sign copied");
				}

			} else {
				sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
				return false;
			}

		} else {
			sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
			return false;
		}

		return false;
	}

}
