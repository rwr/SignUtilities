/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.command.CommandSender;

public class PersoNewsHandler extends NewsHandler {

	public PersoNewsHandler(CommandSender sender, String[] args) {
		super(sender, args);
		permission = "signutilities.commands.snews";

		if (args.length >= 1) {
			action = args[0];
		}
		newsPaperName = sender.getName();

		this.args = new String[args.length - 1];

		for (int i = 1; i < args.length; i++) {
			this.args[i - 1] = args[i];
		}
	}

	@Override
	public boolean execute() {
		return super.execute();
	}

}