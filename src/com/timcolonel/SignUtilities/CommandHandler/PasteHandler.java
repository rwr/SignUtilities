/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SUEditor;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.SignUtilitiesClipBoard;

public class PasteHandler extends CommandHandler {
	public PasteHandler(CommandSender sender, String[] args) {
		super(sender, args);
		permission = "signutilities.commands.spaste";
	}

	@Override
	public boolean execute() {
		// Permissions
		if (!hasPermission()) {
			return false;
		}

		// Get the clipboard of the player
		if (!SignUtilities.instance.playerClipBoard.containsKey((Player) sender)) {
			sender.sendMessage(ChatColor.RED + "No content in clipboard. Copy a sign first by selecting it (right-click) and using the /scopy command.");
			return false;
		}
		SignUtilitiesClipBoard cb = SignUtilities.instance.playerClipBoard.get((Player) sender);

		if (SignUtilities.instance.signSelMgr.hasPlayerSelected((Player) sender)) {

			// Check if the sign is there
			Sign s = (Sign) SignUtilities.instance.signSelMgr.getSelection((Player) sender).getState();
			if (s.getType() == Material.SIGN_POST || s.getType() == Material.WALL_SIGN) {

				SUEditor suEditor = new SUEditor();

				if (args.length == 0) {

					for (int i = 0; i < cb.lines.size(); i++) {
						int lineNb = cb.lines.get(i);
						String line = cb.content.get(i);
						suEditor.changeText(s, line, lineNb, (Player) sender);

					}
					SignUtilities.instance.playerClipBoard.get((Player) sender).paste = true;
					sender.sendMessage(ChatColor.BLUE + "Content pasted");

				} else if (args.length == 1) {
					// Copy only the specified line
					try {
						int l = Integer.parseInt(args[0]);
						l--;

						// Check if the line number is real
						if (l < 0 || l > 4) {
							sender.sendMessage(ChatColor.RED + "There are only four lines on a sign");
							return false;
						}

						// If the player copied more than one line, ignore the argument
						if (cb.lines.size() != 1) {
							for (int i = 0; i < cb.lines.size(); i++) {
								int lineNb = cb.lines.get(i);
								String line = cb.content.get(i);
								suEditor.changeText(s, line, lineNb, (Player) sender);

							}
						} else {
							String line = cb.content.get(0);
							suEditor.changeText(s, line, l, (Player) sender);

						}

						SignUtilities.instance.playerClipBoard.get((Player) sender).paste = true;

					} catch (Exception e) {
						sender.sendMessage(ChatColor.RED + "Number expected");
						return false;
					}
					sender.sendMessage(ChatColor.BLUE + "Content pasted");

				} else {
					// Too many args
					sender.sendMessage(ChatColor.RED + "Too many arguments");
					return false;
				}

			} else {

				sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
				return false;
			}

		} else {
			sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
			return false;
		}

		return false;
	}
}
