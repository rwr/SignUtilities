/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignUtilities;

public class CommandHandler {
	protected CommandSender sender;
	protected String args[];
	protected String permission;

	// ////////////////////////////////////////////////////////////////////////
	// CONSTRUCTOR //
	// ////////////////////////////////////////////////////////////////////////

	public CommandHandler(CommandSender sender, String args[]) {
		permission = "signutilities.commands.*";
		this.sender = sender;
		this.args = args;
	}

	public boolean execute() {
		return true;
	}

	public boolean hasPermission() {
		if (sender instanceof Player) {
			if (SignUtilities.instance.pluginsMgr.hasPermission((Player) sender, permission)) {
				return true;
			} else {
				sender.sendMessage(ChatColor.RED + "You don't have access to that command!");
				return false;
			}
		} else {
			return true;
		}
	}

	// ////////////////////////////////////////////////////////////////////////
	// SETTERS-GETTERS //
	// ////////////////////////////////////////////////////////////////////////

	public CommandSender getSender() {
		return sender;
	}

	public void setSender(CommandSender sender) {
		this.sender = sender;
	}

	public String[] getArgs() {
		return args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}

}
