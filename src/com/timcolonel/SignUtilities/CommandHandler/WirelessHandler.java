/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.Selection;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.WirelessGroup;

public class WirelessHandler extends CommandHandler {

	public WirelessHandler(CommandSender sender, String[] args) {
		super(sender, args);
		permission = "signutilities.commands.wireless";
	}

	@Override
	public boolean execute() {
		if (!hasPermission()) {
			return false;
		}

		if (args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Too few arguments");
			return false;

		} else {
			Selection sel = SignUtilities.instance.linkMgr.getSelection((Player) sender);
			if (sel == null) {
				sender.sendMessage(ChatColor.RED + "Select a region first");
				return true;
			}

			if (args[0].equalsIgnoreCase("add")) {
				WirelessGroup wg = new WirelessGroup();

				if (!wg.fromSelection(sel)) {
					sender.sendMessage(ChatColor.RED + "A least one the the selection is not a sign or an activator!");
					return true;
				}

				SignUtilities.instance.wlSignMgr.addConnection(wg);
				sender.sendMessage(ChatColor.BLUE + "New link created");

			} else if (args[0].equalsIgnoreCase("del")) {

				WirelessGroup wg = new WirelessGroup();

				if (!wg.fromSelection(sel)) {
					sender.sendMessage(ChatColor.RED + "A least one the the selaction is not a sign or an activator!");
					return true;
				}

				if (SignUtilities.instance.wlSignMgr.removeConnection(wg)) {
					sender.sendMessage(ChatColor.BLUE + "This link has been deleted");
				} else {
					sender.sendMessage(ChatColor.RED + "This combination doesn't exist!");
				}

			} else {
				return false;
			}

		}

		return true;
	}
}
