/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;
import org.bukkit.block.Block;

public class RotateHandler extends CommandHandler {
	public RotateHandler(CommandSender sender, String[] args) {
		super(sender, args);
		permission = "signutilities.commands.srotate";
	}

	@Override
	public boolean execute() {
		// Permissions
		if (!hasPermission()) {
			return false;
		}

		if (SignUtilities.instance.signSelMgr.hasPlayerSelected((Player) sender)) {
			Block signBlock = SignUtilities.instance.signSelMgr.getSelection((Player) sender);

			// Check if the sign is there
			if (Utils.isASign(signBlock)) {

				// Rotate sign
				if (args.length == 0) {
					sender.sendMessage(ChatColor.RED + "Enter a number from 0 to 360 to rotate the sign by.");
					return true;

				} else if (args.length > 1) {
					// Means there are too many args
					sender.sendMessage(ChatColor.RED + "Too many arguments");
					return false;

				} else {
					try {
						int arg = Integer.parseInt(args[0]);
						
						// make sure data is within range
						if (arg < 0 || arg > 360) {
							sender.sendMessage(ChatColor.RED + "Enter a number from 0 to 360 to rotate the sign by.");
							return true;
						}
						
						// signs rotate from 0 to 15
						int addData = (int) Math.round(arg / 22.5);
						
						// old/current sign data
						int oldData = signBlock.getData();

						byte newData = (byte) (oldData + addData);
						
						signBlock.setData(newData);

					} catch (Exception e) {
						sender.sendMessage(ChatColor.RED + "Number Expected");
						return false;
					}

					sender.sendMessage(ChatColor.BLUE + "Sign rotated");
				}

			} else {
				sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
				return false;
			}

		} else {
			sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
			return false;
		}

		return false;
	}

}
