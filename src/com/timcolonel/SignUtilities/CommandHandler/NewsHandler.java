/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignUtilities;

public class NewsHandler extends CommandHandler {

	protected String action;
	protected String newsPaperName;
	protected String newsTitle;
	protected String newsContent;

	public NewsHandler(CommandSender sender, String[] args) {
		super(sender, args);

		permission = "signutilities.commands.snewsp";

		if (args.length >= 2) {
			action = args[0];
			newsPaperName = args[1];
		}

		if (args.length >= 3) {
			this.args = new String[args.length - 2];

			for (int i = 2; i < args.length; i++) {
				this.args[i - 2] = args[i];
			}
		}
	}

	@Override
	public boolean execute() {
		if (!hasPermission()) {
			return false;
		}

		if (newsPaperName.isEmpty() || action.isEmpty()) {
			sender.sendMessage(ChatColor.RED + "Too few arguments");
			return false;
		}

		if (action.equalsIgnoreCase("ADD")) {
			if (args.length >= 2) {
				newsTitle = args[0];
				newsContent = "";
				for (int i = 1; i < args.length; i++) {
					newsContent += args[i] + " ";
				}

			} else {
				sender.sendMessage(ChatColor.RED + "Too few arguments for adding news");
			}

			SignUtilities.instance.newsMgr.addNews(newsPaperName, newsTitle, newsContent);
			sender.sendMessage(ChatColor.BLUE + "News " + ChatColor.GREEN + "\"" + newsTitle + "\"" + ChatColor.BLUE + " successfully added to the newspaper " + ChatColor.GREEN + "\"" + newsPaperName + "\"");
			return true;

		} else if (action.equalsIgnoreCase("DEL")) {
			if (args.length >= 1) {
				newsTitle = args[0];
			} else {
				sender.sendMessage(ChatColor.RED + "Too few arguments for deleting news");
			}

			if (SignUtilities.instance.newsMgr.delNews(newsPaperName, newsTitle)) {
				sender.sendMessage(ChatColor.BLUE + "News " + ChatColor.GREEN + "\"" + newsTitle + "\"" + ChatColor.BLUE + " successfully deleted in the newspaper " + ChatColor.GREEN + "\"" + newsPaperName + "\"");
			} else {
				sender.sendMessage(ChatColor.RED + "News " + ChatColor.DARK_RED + "\"" + newsTitle + "\"" + ChatColor.RED + " does not exist in " + ChatColor.DARK_RED + "\"" + newsPaperName + "\"");
			}

			return true;
		} else if (action.equalsIgnoreCase("LIST")) {
			if (args.length >= 1) {
				try {
					SignUtilities.instance.newsMgr.listNews(newsPaperName, (Player) sender, Integer.parseInt(args[0]));
				} catch (Exception e) {
					sender.sendMessage(ChatColor.RED + "Argument 3 must be a page number");
					return false;
				}
			} else {
				SignUtilities.instance.newsMgr.listNews(newsPaperName, (Player) sender, 1);
			}
			return true;
		} else {
			return false;
		}

	}

}
