/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignUtilities;

public class CommandManager {

	public Boolean manage(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		// Check if the player is trying to edit a sign in a protected region
		if (!SignUtilities.instance.pluginsMgr.canEditSign(sender.getName())) {
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("setcolor")) {
			if (sender instanceof Player) {
				return setColor(sender, cmd, commandLabel, args);
			} else {
				sender.sendMessage("Only players can use that command!");
				return true;
			}

		} else if (cmd.getName().equalsIgnoreCase("settext")) {
			if (sender instanceof Player) {
				return setText(sender, cmd, commandLabel, args);
			} else {
				sender.sendMessage("Only players can use that command!");
				return true;
			}

		} else if (cmd.getName().equalsIgnoreCase("scopy")) {
			if (sender instanceof Player) {
				return copy(sender, cmd, commandLabel, args);
			} else {
				sender.sendMessage("Only players can use that command!");
				return true;
			}

		} else if (cmd.getName().equalsIgnoreCase("spaste")) {
			if (sender instanceof Player) {
				return paste(sender, cmd, commandLabel, args);
			} else {
				sender.sendMessage("Only players can use that command!");
				return true;
			}

		} else if (cmd.getName().equalsIgnoreCase("snews")) {
			if (args.length == 0) {
				sender.sendMessage(ChatColor.RED + "Too few arguments");
				return false;
			}
			return news(sender, cmd, commandLabel, args);

		} else if (cmd.getName().equalsIgnoreCase("snewsp")) {
			if (args.length == 0) {
				sender.sendMessage(ChatColor.RED + "Too few arguments");
				return false;
			}
			return newsP(sender, cmd, commandLabel, args);

		} else if (cmd.getName().equalsIgnoreCase("swa")) {
			if (sender instanceof Player) {
				return wireless(sender, cmd, commandLabel, args);
			} else {
				sender.sendMessage("Only players can use that command!");
				return true;
			}

		} else if (cmd.getName().equalsIgnoreCase("srotate")) {
			if (sender instanceof Player) {
				return rotate(sender, cmd, commandLabel, args);
			} else {
				sender.sendMessage("Only players can use that command!");
				return true;
			}

		} else {
			return false;
		}

	}

	/********************************************************************************************
	 * Manage the setColor command
	 ********************************************************************************************/

	public Boolean setColor(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		SetColorHandler handler = new SetColorHandler(sender, args);
		handler.execute();
		return true;
	}

	/********************************************************************************************
	 * Manage the setText command
	 ********************************************************************************************/
	public Boolean setText(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		SetTextHandler handler = new SetTextHandler(sender, args);
		handler.execute();
		return true;
	}

	/********************************************************************************************
	 * Manage the scopy command
	 ********************************************************************************************/
	public Boolean copy(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		CopyHandler handler = new CopyHandler(sender, args);
		handler.execute();
		return true;

	}

	/********************************************************************************************
	 * Manage the spaste command
	 ********************************************************************************************/
	public Boolean paste(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		PasteHandler handler = new PasteHandler(sender, args);
		handler.execute();
		return true;
	}

	/********************************************************************************************
	 * Manage the news command
	 ********************************************************************************************/

	public Boolean news(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		PersoNewsHandler handler = new PersoNewsHandler(sender, args);
		handler.execute();
		return true;
	}

	/********************************************************************************************
	 * Manage the newsP command
	 *********************************************************************************************/

	public Boolean newsP(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		NewsHandler handler = new NewsHandler(sender, args);
		handler.execute();
		return true;
	}

	/********************************************************************************************
	 * Manage the wireless command
	 *********************************************************************************************/
	public Boolean wireless(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		WirelessHandler handler = new WirelessHandler(sender, args);
		handler.execute();
		return true;

	}

	/********************************************************************************************
	 * Manage the srotate command
	 ********************************************************************************************/
	public Boolean rotate(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		RotateHandler handler = new RotateHandler(sender, args);
		handler.execute();
		return true;

	}
}
