/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SUEditor;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;

public class SetTextHandler extends CommandHandler {

	public SetTextHandler(CommandSender sender, String[] args) {
		super(sender, args);
		permission = "signutilities.commands.settext";
	}

	@Override
	public boolean execute() {
		if (!hasPermission()) {
			return false;
		}

		if (SignUtilities.instance.signSelMgr.hasPlayerSelected((Player) sender)) {

			if (args.length == 0) {
				sender.sendMessage(ChatColor.RED + "Too few arguments");
				return false;

			} else {
				if (Utils.isASign(SignUtilities.instance.signSelMgr.getSelection((Player) sender))) {

					SUEditor suEditor = new SUEditor();

					try {
						int l = Integer.parseInt(args[0]);

						// Check if the line number is real
						if (l < 0 || l > 4) {
							sender.sendMessage(ChatColor.RED + "There are only four lines on a sign");
							return false;
						}

						String text = "";
						for (int i = 1; i < args.length; i++) {

							text += args[i] + " ";
						}

						if (suEditor.changeText((Sign) SignUtilities.instance.signSelMgr.getSelection((Player) sender).getState(), text, l - 1, (Player) sender)) {
							sender.sendMessage(ChatColor.BLUE + "Set line " + args[0] + " with the text '" + text + "'");
							return true;
						}

					} catch (Exception e) {

						// Get all the text form the args
						String text = "";
						for (int i = 0; i < args.length; i++) {
							text += args[i] + " ";
						}

						if (suEditor.changeText((Sign) SignUtilities.instance.signSelMgr.getSelection((Player) sender).getState(), text, 0, (Player) sender)) {
							sender.sendMessage(ChatColor.BLUE + "Set text sucessfully");
							return true;
						}
					}
				} else {
					sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
					return false;
				}

			}

		} else {
			sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
			return false;
		}

		return false;
	}

}
