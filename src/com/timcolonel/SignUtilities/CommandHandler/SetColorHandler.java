/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.CommandHandler;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;

public class SetColorHandler extends CommandHandler {

	public SetColorHandler(CommandSender sender, String[] args) {
		super(sender, args);
		permission = "signutilities.commands.setcolor";
	}

	@Override
	public boolean execute() {

		// If there are no args, set all the lines colors to black

		if (!hasPermission()) {
			return false;
		}

		if (SignUtilities.instance.signSelMgr.hasPlayerSelected((Player) sender)) {

			// Check if the sign is there
			if (Utils.isASign(SignUtilities.instance.signSelMgr.getSelection((Player) sender))) {

				// Check if the player is trying to edit a sign in a protected region
				if (!SignUtilities.instance.pluginsMgr.canEditSign(sender.getName())) {
					return true;
				}

				if (args.length == 0) {
					if (SignUtilities.instance.colorsMgr.colorSign((Sign) SignUtilities.instance.signSelMgr.getSelection((Player) sender).getState(), "BLACK", 0)) {
						sender.sendMessage(ChatColor.BLUE + "Sign color reset to black");
						return true;
					} else {
						return false;
					}

				} else if (args.length == 1) {

					// If there is only one set all the line to the color specified
					if (SignUtilities.instance.colorsMgr.colorSign((Sign) SignUtilities.instance.signSelMgr.getSelection((Player) sender).getState(), args[0].toUpperCase(), 0)) {
						sender.sendMessage(ChatColor.BLUE + "Sign color set to " + args[0].toUpperCase());
						return true;
					} else {
						sender.sendMessage(ChatColor.RED + "This color doesn't exist: " + args[0].toUpperCase());
						return false;
					}

				} else if (args.length == 2) {
					try {
						int l = Integer.parseInt(args[1]);

						// Check if the line number is real
						if (l < 0 || l > 4) {
							sender.sendMessage(ChatColor.RED + "There are only four lines on a sign");
							return false;
						}

						if (SignUtilities.instance.colorsMgr.colorSign((Sign) SignUtilities.instance.signSelMgr.getSelection((Player) sender).getState(), args[0].toUpperCase(), l)) {
							sender.sendMessage(ChatColor.BLUE + "Set line " + args[1] + " in " + args[0].toUpperCase());
							return true;
						} else {
							sender.sendMessage(ChatColor.RED + "This color doesn't exist: " + args[0].toUpperCase());
							return false;
						}

					} catch (Exception e) {
						sender.sendMessage(ChatColor.RED + "Usage: /setcolor color line#");
						return false;
					}

				} else {
					// Too many args
					sender.sendMessage(ChatColor.RED + "Too many args");
					return false;
				}

			} else {
				sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
				return false;
			}

		} else {
			sender.sendMessage(ChatColor.RED + "No sign selected (The sign may have been removed)");
			return false;
		}

	}

}
