/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Manager;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.Location;
import org.bukkit.entity.Minecart;

import com.timcolonel.SignUtilities.SUMinecart;
import com.timcolonel.SignUtilities.SignUtilities;

public class SUMinecartManager {
	public final HashMap<Minecart, SUMinecart> minecarts = new HashMap<Minecart, SUMinecart>();

	public SUMinecart isMinecartAt(Location l) {

		for (Minecart m : minecarts.keySet()) {
			if (l.distance(m.getLocation()) < 1) {
				return minecarts.get(m);
			}
		}

		return null;
	}

	public void minecartDestroyed(Minecart m) {
		// Add a new minecart item in the native chest
		minecarts.get(m).minecartDestroyed(true);

		// Remove the minecart from the SU minecarts list
		minecarts.remove(m);

		// Destroy the entity
		m.remove();
	}

	public boolean addMinecart(Minecart minecart, SUMinecart su_minecart) {
		minecarts.put(minecart, su_minecart);
		return true;
	}

	public void removeAllMinecarts() {
		if (minecarts.size() > 0) {
			Iterator<Entry<Minecart, SUMinecart>> carts = minecarts.entrySet().iterator();
			while (carts.hasNext()) {
				Map.Entry<Minecart, SUMinecart> cart = (Map.Entry<Minecart, SUMinecart>) carts.next();

				// put back in the chest
				SignUtilities.instance.minecartMgr.minecartDestroyed(minecarts.get(cart.getKey()).getMinecart());

			}
			minecarts.clear();
		}
	}
}
