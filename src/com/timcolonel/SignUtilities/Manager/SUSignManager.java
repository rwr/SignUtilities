/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Manager;

import java.util.HashMap;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignAction;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.SignCommands.ActivatorSign;
import com.timcolonel.SignUtilities.SignCommands.CmdLinkSign;
import com.timcolonel.SignUtilities.SignCommands.CmdSign;
import com.timcolonel.SignUtilities.SignCommands.MinecartRemoveSign;
import com.timcolonel.SignUtilities.SignCommands.MinecartSign;
import com.timcolonel.SignUtilities.SignCommands.BoatRemoveSign;
import com.timcolonel.SignUtilities.SignCommands.BoatSign;
import com.timcolonel.SignUtilities.SignCommands.NewsSign;
import com.timcolonel.SignUtilities.SignCommands.WebLinkSign;

public class SUSignManager {
	public final HashMap<String, Block> tempSigns = new HashMap<String, Block>();

	public boolean runSignCommand(Player p, Block signBlock, Block clickedBlock, BlockFace face, SignAction action) {
		Sign sign = (Sign) signBlock.getState();
		String playerName = p.getName();

		boolean commandSign = false;

		// Check if the first line of the sign is a Dark_blue, meaning that it is a command sign or if the user wants to see the information

		if (sign.getLine(0).contains("info:")) {

			NewsSign newsSign = new NewsSign(p, sign, clickedBlock, face);
			newsSign.run(playerName);
			commandSign = true;
		}

		String key = sign.getLine(0);

		if (key.contains("[cmd]") && SignUtilities.instance.pluginsMgr.hasPermission(p, "signutilities.signs.cmd.custom.use")) {
			CmdSign cmdSign = new CmdSign(p, sign, clickedBlock);
			cmdSign.run(playerName);
			commandSign = true;

		} else if (key.contains("[minecart]")) {
			minecartCommand(p, clickedBlock, sign);
			commandSign = true;

		} else if (key.contains("[minecart:rem]")) {
			minecartRemoveCommand(p, clickedBlock, sign);
			commandSign = true;

		} else if (key.contains("[boat]")) {
			boatCommand(p, clickedBlock, sign);
			commandSign = true;

		} else if (key.contains("[boat:rem]")) {
			boatRemoveCommand(p, clickedBlock, sign);
			commandSign = true;

		} else if (key.contains("[activator]")) {
			activateBlockAroundSign(p, sign, clickedBlock);
			commandSign = true;
		} else {

			for (String str : sign.getLines()) {
				if (str.contains("[") && str.contains("]")) {
					CmdLinkSign cmdLinkSign = new CmdLinkSign(p, sign, clickedBlock, action);

					if (cmdLinkSign.run(playerName)) {
						commandSign = true;
					}

					break;
				}

			}

			for (String str : sign.getLines()) {
				if (str.contains("(") && str.contains(")")) {
					WebLinkSign webLinkSign = new WebLinkSign(p, sign, clickedBlock, action);

					if (webLinkSign.run(playerName)) {
						commandSign = true;
					}

					break;
				}
			}
		}

		return commandSign;
	}

	public void minecartCommand(Player p, Block clickedBlock, Sign sign) {
		MinecartSign minecartSign = new MinecartSign(p, sign, clickedBlock);
		minecartSign.run(p.getName());
	}

	public void minecartRemoveCommand(Player p, Block clickedBlock, Sign sign) {
		MinecartRemoveSign removeSign = new MinecartRemoveSign(p, sign, clickedBlock);
		removeSign.run(p.getName());
	}

	public void boatCommand(Player p, Block clickedBlock, Sign sign) {
		BoatSign boatSign = new BoatSign(p, sign, clickedBlock);
		boatSign.run(p.getName());
	}

	public void boatRemoveCommand(Player p, Block clickedBlock, Sign sign) {
		BoatRemoveSign removeSign = new BoatRemoveSign(p, sign, clickedBlock);
		removeSign.run(p.getName());
	}

	public void activateBlockAroundSign(Player p, Sign sign, Block block) {
		ActivatorSign activator = new ActivatorSign(p, sign, block);
		activator.run(p.getName());
	}
}
