/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Manager;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.Selection;

public class PlayerLinkSelectionManager {

	public final HashMap<Player, Selection> savedBlockSelection = new HashMap<Player, Selection>();

	// ///////////////////////////////////////////////////////////////////////////////////////////////////
	// SELECTION //
	// ///////////////////////////////////////////////////////////////////////////////////////////////////
	public Selection getSelection(Player p) {
		if (savedBlockSelection.containsKey(p)) {
			Selection s = savedBlockSelection.get(p);
			if (s.getPointA() != null && s.getPointB() != null) {
				return s;
			}
		}
		return null;
	}

	public void setSelection(Player p, Selection sel) {
		savedBlockSelection.put(p, sel);
	}

	public void setPointA(Player p, Location l) {
		if (savedBlockSelection.containsKey(p)) {
			Selection sel = savedBlockSelection.get(p);
			sel.setPointA(l);
			savedBlockSelection.put(p, sel);
		} else {
			Selection sel = new Selection();
			sel.setPointA(l);
			sel.setWorld(l.getWorld());
			savedBlockSelection.put(p, sel);
		}
	}

	public void setPointB(Player p, Location l) {
		if (savedBlockSelection.containsKey(p)) {
			Selection sel = savedBlockSelection.get(p);
			sel.setPointB(l);
			savedBlockSelection.put(p, sel);

		} else {
			Selection sel = new Selection();
			sel.setPointA(l);
			sel.setWorld(l.getWorld());
			savedBlockSelection.put(p, sel);

		}
	}

}
