/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Manager;

import java.util.HashMap;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class PlayerSignSelectionManager {
	public final HashMap<Player, Block> playerSelection = new HashMap<Player, Block>();

	public boolean hasPlayerSelected(Player player) {
		return playerSelection.containsKey(player);
	}

	public Block getSelection(Player player) {
		return playerSelection.get(player);
	}

	public void addSelection(Player player, Block block) {
		playerSelection.put(player, block);
	}
}
