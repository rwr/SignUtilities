/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.bukkit.configuration.file.FileConfiguration;
import com.timcolonel.SignUtilities.Utils;
import com.timcolonel.SignUtilities.WirelessGroup;
import com.timcolonel.SignUtilities.Files.ConfigFile;

public class WirelessSignManager {

	public final HashMap<String, ArrayList<String>> wirelessSigns = new HashMap<String, ArrayList<String>>();
	public ConfigFile wirelessSignsFile;

	public WirelessSignManager() {

		// Load the file
		wirelessSignsFile = new ConfigFile("WirelessSigns.yml");
		wirelessSignsFile.saveDefaultConfig();
	}

	public boolean blockHasConnection(String key) {
		return wirelessSigns.containsKey(key);
	}

	public ArrayList<String> getSignLinkedTo(String key) {
		return wirelessSigns.get(key);
	}

	public void addConnection(WirelessGroup wg) {

		String a = Utils.blockToString(wg.activator);
		String s = Utils.blockToString(wg.sign);
		if (wirelessSigns.containsKey(a)) {
			ArrayList<String> list = wirelessSigns.get(a);

			if (!list.contains(s)) {
				list.add(s);
				wirelessSigns.put(a, list);
			}

		} else {
			ArrayList<String> list = new ArrayList<String>();
			list.add(s);
			wirelessSigns.put(a, list);
		}
	}

	public boolean removeConnection(WirelessGroup wg) {
		String a = Utils.blockToString(wg.activator);
		String s = Utils.blockToString(wg.sign);

		if (wirelessSigns.containsKey(a)) {
			ArrayList<String> list = wirelessSigns.get(a);

			if (!list.contains(s)) {
				list.remove(s);
				wirelessSigns.put(a, list);
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	public void saveWireLessSigns() {

		Set<String> keys = wirelessSigns.keySet();
		for (String key : keys) {
			wirelessSignsFile.getConfig().set("Connections." + key, wirelessSigns.get(key));
		}

		wirelessSignsFile.saveConfig();
	}

	public void loadWireLessSigns() {
		FileConfiguration config = wirelessSignsFile.getConfig();

		if (config.isConfigurationSection("Connections")) {
			Set<String> keys = config.getConfigurationSection("Connections").getKeys(false);

			if (keys != null) {
				for (String key : keys) {
					ArrayList<String> list = (ArrayList<String>) config.getStringList("Connections." + key);
					wirelessSigns.put(key, list);
				}
			}
		}
	}

}
