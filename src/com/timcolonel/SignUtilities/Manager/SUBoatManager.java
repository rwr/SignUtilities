/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Manager;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.Location;
import org.bukkit.entity.Boat;

import com.timcolonel.SignUtilities.SUBoat;
import com.timcolonel.SignUtilities.SignUtilities;

public class SUBoatManager {
	public final HashMap<Boat, SUBoat> boats = new HashMap<Boat, SUBoat>();

	public SUBoat isBoatAt(Location l) {

		for (Boat b : boats.keySet()) {
			if (l.distance(b.getLocation()) < 1) {
				return boats.get(b);
			}
		}

		return null;
	}

	public void boatDestroyed(Boat b) {
		// Add a new boat item in the native chest
		boats.get(b).boatDestroyed(true);

		// Remove the boat from the SU boats list
		boats.remove(b);

		// Destroy the entity
		b.remove();
	}

	public boolean addBoat(Boat boat, SUBoat su_boat) {
		boats.put(boat, su_boat);
		return true;
	}

	public void removeAllBoats() {
		if (boats.size() > 0) {
			Iterator<Entry<Boat, SUBoat>> carts = boats.entrySet().iterator();
			while (carts.hasNext()) {
				Map.Entry<Boat, SUBoat> cart = (Map.Entry<Boat, SUBoat>) carts.next();

				// put back in the chest
				SignUtilities.instance.boatMgr.boatDestroyed(boats.get(cart.getKey()).getBoat());

			}
			boats.clear();
		}
	}
}
