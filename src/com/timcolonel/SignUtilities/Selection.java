/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities;

import org.bukkit.Location;
import org.bukkit.World;

public class Selection {
	Location pointA;
	Location pointB;
	World world;

	public Selection() {
		pointA = null;
		pointB = null;
		world = null;
	}

	public Selection(Location a, Location b) {
		pointA = a;
		pointB = b;
		world = pointA.getWorld();
	}

	public void setPointA(Location a) {
		pointA = a;
	}

	public void setPointB(Location b) {
		pointB = b;
	}

	public void setWorld(World w) {
		world = w;
	}

	public Location getPointA() {
		return pointA;
	}

	public Location getPointB() {
		return pointB;
	}

	public World getWorld() {
		return world;
	}
}
