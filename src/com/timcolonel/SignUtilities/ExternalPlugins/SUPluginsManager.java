/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.ExternalPlugins;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;
import com.timcolonel.SignUtilities.Response.TransactionResponse;

public class SUPluginsManager {
	public static WorldGuardPlugin wgHandeler;

	public static Permission perms = null;
	public static Economy econ = null;

	private boolean setupEconomy() {

		RegisteredServiceProvider<Economy> rsp = SignUtilities.instance.getServer().getServicesManager().getRegistration(Economy.class);

		if (rsp == null) {
			return false;
		}

		econ = rsp.getProvider();

		return econ != null;
	}

	public boolean setupPlugins() {
		if (SignUtilities.instance.getServer().getPluginManager().getPlugin("Vault") == null) {
			SignUtilities.instance.logger.severe("Disabling due to no Vault plugin found!");
			return false;
		}

		if (!setupEconomy()) {
			SignUtilities.instance.logger.info("No economy plugin found! Some features will be disabled.");
		}

		Plugin wgPlugin = SignUtilities.instance.getServer().getPluginManager().getPlugin("WorldGuard");
		if (wgPlugin == null) {
			SignUtilities.instance.logger.info("World Guard system not detected!");
		} else {
			wgHandeler = (WorldGuardPlugin) wgPlugin;
		}

		return true;
	}

	public boolean hasPermission(Player player, String s) {
		//System.out.println("PERMISSION: " + player.hasPermission(s) + " : " + s);

		return player.hasPermission(s);
	}

	public boolean canColorSign(Player player) {
		if (!hasPermission(player, "signutilities.commands.setcolor")) {
			return false;
		}

		if (wgHandeler != null) {
			if (!wgHandeler.getGlobalRegionManager().canBuild(player, SignUtilities.instance.signSelMgr.getSelection(player))) {
				player.sendMessage(ChatColor.RED + "You can't edit this sign");
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public boolean canEditSign(String playerName) {
		Player player = SignUtilities.instance.getServer().getPlayer(playerName);

		if (!hasPermission(player, "signutilities.commands.settext")) {
			return false;
		}

		Block block = SignUtilities.instance.signSelMgr.getSelection(player);
		
		// can't edit a block if there is no block
		if(block == null) {
			player.sendMessage(ChatColor.RED + "You have to use (right-click) a sign to select it before you can edit it.");
			return false;
		}

		// check worldguard
		if (wgHandeler != null) {
			if (!wgHandeler.getGlobalRegionManager().canBuild(player, block)) {
				player.sendMessage(ChatColor.RED + "You can't edit this sign");
				return false;
			} else {
				return true;
			}

		} else {
			return Utils.canEdit(player.getName(), block);
		}
	}

	public TransactionResponse ecoPay(Player sender, String reciverName, double amount) {
		if (econ != null && amount > 0) {
			EconomyResponse withdrawResponse = econ.withdrawPlayer(sender.getName(), amount);

			if (withdrawResponse.transactionSuccess()) {
				EconomyResponse depositResponse = econ.depositPlayer(reciverName, amount);

				if (depositResponse.transactionSuccess()) {
					return new TransactionResponse(true, "Transaction success", amount);
				} else {
					return new TransactionResponse(false, depositResponse.errorMessage, 0);
				}

			} else {
				return new TransactionResponse(false, withdrawResponse.errorMessage, 0);
			}

		} else {
			if (amount > 0)
				return new TransactionResponse(false, "Transaction error", 0);
			else {
				return new TransactionResponse(false, "Transaction amount is 0", amount);
			}
		}
	}
}
