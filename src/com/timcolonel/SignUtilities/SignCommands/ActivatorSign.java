/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.SignCommands;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Dispenser;
import org.bukkit.block.Dropper;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;

/**************************************************************
 * This sign will activate all the blocks around it
 **************************************************************/
public class ActivatorSign extends SignCommand {
	public ActivatorSign(Player player, Sign sign, Block block) {
		super(player, sign, block);
		this.permission = "signutilities.signs.activator.use";
	}

	@Override
	public boolean run(String playerName) {

		if (!hasPermission()) {
			return true;
		}

		Block mainBlock = sign.getBlock();

		// check if they can do stuff here
		if (SignUtilities.instance.config.getConfig().getBoolean("safeSigns.activatorSign")) {
			if (!Utils.canEdit(playerName, mainBlock)) {
				return false;
			}
		}

		activateBlock(mainBlock, 1, 0, 0);
		activateBlock(mainBlock, -1, 0, 0);
		activateBlock(mainBlock, 0, 1, 0);
		activateBlock(mainBlock, 0, -1, 0);
		activateBlock(mainBlock, 0, 0, 1);
		activateBlock(mainBlock, 0, 0, -1);

		return true;
	}

	public void activateBlock(Block main, float x, float y, float z) {
		Location loc = main.getLocation();
		loc.add(x, y, z);
		Block blockToActivate = loc.getBlock();

		BlockState state = blockToActivate.getState();

		switch (state.getTypeId()) {

		// If its a dispenser
		case 23:
			Dispenser dispenser = (Dispenser) state;
			dispenser.dispense();
			break;

		// If its a dropper
		case 158:
			Dropper dropper = (Dropper) state;
			dropper.drop();
			break;

		}

	}
}
