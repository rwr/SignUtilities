/**
 * SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 * Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.timcolonel.SignUtilities.SignCommands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SUMinecart;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;
import com.timcolonel.SignUtilities.Vector;
import com.timcolonel.SignUtilities.Response.TransactionResponse;

public class MinecartSign extends SignCommand {

	public MinecartSign(Player player, Sign sign, Block block) {
		super(player, sign, block);
		this.permission = "signutilities.signs.minecart.use";
	}

	@Override
	public boolean run(String playerN) {
		if (!hasPermission()) {
			return true;
		}

		// Create the block where the minecart will spawn or be removed, by default the sign
		Block positionBlock = sign.getBlock();

		// check if they can do stuff here
		if (SignUtilities.instance.config.getConfig().getBoolean("safeSigns.minecartSign")) {
			if (!Utils.canEdit(playerN, positionBlock)) {
				return false;
			}
		}

		String playerName = sign.getLine(1);
		double cost = 0;
		try {
			cost = Integer.parseInt(sign.getLine(2));
		} catch (Exception e) {
		}

		Vector relative = new Vector();

		if (sign.getLine(3).isEmpty()) {
			// default cart position if last line of sign is blank

			// find rail around chest
			if (sign.getBlock().getRelative(1, -1, 0).getType() == Material.RAILS) {
				relative.extractVector("1;-1;0");
			} else if (sign.getBlock().getRelative(-1, -1, 0).getType() == Material.RAILS) {
				relative.extractVector("-1;-1;0");
			} else if (sign.getBlock().getRelative(0, -1, 1).getType() == Material.RAILS) {
				relative.extractVector("0;-1;1");
			} else if (sign.getBlock().getRelative(0, -1, -1).getType() == Material.RAILS) {
				relative.extractVector("0;-1;-1");
			} else {
				relative.extractVector("0;0;0");
			}

		} else {
			// get cart position from bottom line of the sign
			relative.extractVector(sign.getLine(3));
		}

		int X = positionBlock.getX();
		int Y = positionBlock.getY();
		int Z = positionBlock.getZ();

		Location minecartLocation = new Location(player.getWorld(), X + 0.5 + relative.x, Y + relative.y, Z + 0.5 + relative.z);
		Block chestBlock = sign.getBlock().getRelative(0, -1, 0);

		// Load the minecart at the spawnPostition to see if there is one
		SUMinecart m = SignUtilities.instance.minecartMgr.isMinecartAt(minecartLocation);

		// If there is no Minecart, continue the process of creating a minecart
		if (m == null) {

			// Check if there is a chest containing the minecarts
			if (chestBlock.getType() == Material.CHEST) {

				// Check if there are some minecarts remaining in the chest
				if (((Chest) chestBlock.getState()).getInventory().contains(Material.MINECART.getId())) {

					boolean spawnMinecart = false;
					if (cost > 0) {

						// Make the player pay the price of use.
						TransactionResponse response = SignUtilities.instance.pluginsMgr.ecoPay(player, playerName, cost);

						if (response.success()) {
							spawnMinecart = true;
						} else {
							player.sendMessage(ChatColor.RED + response.getErrorMessage());
						}
					} else {
						spawnMinecart = true;
					}

					if (spawnMinecart) {
						// Spawn the minecart
						Minecart minecart = player.getWorld().spawn(minecartLocation, Minecart.class);

						// Create the signUtilities MinecartObject that will allow to the Minecart to go back to is initial chest when destroyed
						SUMinecart su_minecart = new SUMinecart(minecart, (Chest) chestBlock.getState(), SignUtilities.instance);
						SignUtilities.instance.minecartMgr.addMinecart(minecart, su_minecart);
					}

				} else {
					player.sendMessage(ChatColor.RED + "No Minecart in chest");
				}

			} else {
				player.sendMessage(ChatColor.RED + "No chest");
			}

		} else {
			SignUtilities.instance.minecartMgr.minecartDestroyed(m.getMinecart());
		}

		return true;
	}
}