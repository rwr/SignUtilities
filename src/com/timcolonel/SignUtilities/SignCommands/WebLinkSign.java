/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.SignCommands;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.Colors;
import com.timcolonel.SignUtilities.SignAction;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;

public class WebLinkSign extends SignCommand {
	protected SignAction action;
	protected String links[] = new String[4];
	protected String url;
	int selected = 0;

	public WebLinkSign(Player player, Sign sign, Block block, SignAction action) {
		super(player, sign, block);
		this.permission = "signutilities.signs.web.use";

		this.action = action;
		int i = 0;
		for (String line : sign.getLines()) {
			links[i] = "";

			if (line.contains("(") && line.contains(")")) {

				if (Colors.ContainColor(line, ChatColor.DARK_BLUE)) {
					selected = i;
				}

				String str;
				str = Colors.removeColor(line);
				str = str.substring(1, str.length() - 1);

				if (SignUtilities.instance.config.webLinks.containsKey(str)) {
					links[i] = str;
				}
			}

			i++;
		}

		if (!links[selected].equalsIgnoreCase("")) {
			url = SignUtilities.instance.config.webLinks.get(links[selected]);
		} else {
			url = "";
		}
	}

	@Override
	public boolean run(String playerName) {
		if (!hasPermission()) {
			return true;
		}

		// check if they can do stuff here
		if (SignUtilities.instance.config.getConfig().getBoolean("safeSigns.webLinkSign")) {
			if (!Utils.canEdit(playerName, sign.getBlock())) {
				return false;
			}
		}

		if (!url.equalsIgnoreCase("")) {

			// If left click on sign, execute
			if (action == SignAction.LeftClick) {
				displayUrl();

			} else if (action == SignAction.RightClick) {
				// If right click change position
				int prevSel = selected;
				selected++;
				selected %= 4;

				while (links[selected].equalsIgnoreCase("")) {
					selected++;
					selected %= 4;

				}

				// Remove the color on the previous selection
				sign.setLine(prevSel, Colors.removeColor(sign.getLine(prevSel)));

				// set the color to the new selection
				sign.setLine(selected, ChatColor.DARK_BLUE + sign.getLine(selected));
				sign.update();

				url = SignUtilities.instance.config.webLinks.get(links[selected]);
			}

			return true;

		} else {
			return false;
		}

	}

	private void displayUrl() {
		player.sendMessage(url);
	}
}