/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.SignCommands;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SUBoat;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;
import com.timcolonel.SignUtilities.Vector;

public class BoatRemoveSign extends SignCommand {

	public BoatRemoveSign(Player player, Sign sign, Block block) {
		super(player, sign, block);
		this.permission = "signutilities.signs.boat.use";
	}

	@Override
	public boolean run(String playerName) {
		if (!hasPermission()) {
			return true;
		}

		// Create the block where the boat will be removed, by default the sign
		Block positionBlock = sign.getBlock();

		// check if they can do stuff here
		if (SignUtilities.instance.config.getConfig().getBoolean("safeSigns.boatRemovalSign")) {
			if (!Utils.canEdit(playerName, positionBlock)) {
				return false;
			}
		}

		Vector relative = new Vector();
		if (!sign.getLine(3).isEmpty()) {
			relative.extractVector(sign.getLine(3));
		}

		int X = positionBlock.getX();
		int Y = positionBlock.getY();
		int Z = positionBlock.getZ();
		Location boatLocation = new Location(player.getWorld(), X + 0.5 + relative.x, Y + relative.y, Z + 0.5 + relative.z);

		// Load the boat at the spawnPostition to see if there is one
		SUBoat m = SignUtilities.instance.boatMgr.isBoatAt(boatLocation);
		if (m != null) {
			SignUtilities.instance.boatMgr.boatDestroyed(m.getBoat());
			return true;
		} else {
			return false;
		}
	}
}