/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.SignCommands;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.Colors;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;

public class CmdSign extends SignCommand {
	protected String cmdStr;
	private Block block;

	public CmdSign(Player player, Sign sign, Block block) {

		super(player, sign, block);
		this.permission = "signutilities.signs.cmd.use";
		cmdStr = "";

		for (int i = 1; i < 4; i++) {
			cmdStr += Colors.removeColor(sign.getLine(i)) + " ";
		}

		this.block = block;
	}

	@Override
	public boolean run(String playerName) {
		if (!hasPermission()) {
			return true;
		}

		// check if they can do stuff here
		if (SignUtilities.instance.config.getConfig().getBoolean("safeSigns.cmdSign")) {
			if (!Utils.canEdit(playerName, block)) {
				return false;
			}
		}

		executeCommand();

		return true;
	}

	public void executeCommand() {
		if (cmdStr.toLowerCase().contains("[player]")) {
			String[] ls = cmdStr.split(" ");
			cmdStr = "";

			for (int i = 0; i < ls.length; i++) {

				if (ls[i].equalsIgnoreCase("[player]")) {
					ls[i] = player.getName();
				}

				cmdStr += ls[i] + " ";
			}
		}

		player.performCommand(cmdStr);
	}
}
