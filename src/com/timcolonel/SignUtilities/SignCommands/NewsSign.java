/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.SignCommands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SUEditor;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;

public class NewsSign extends SignCommand {
	protected BlockFace face;

	public NewsSign(Player player, Sign sign, Block block, BlockFace face) {
		super(player, sign, block);
		this.face = face;
		this.permission = "signutilities.signs.news.use";
	}

	@Override
	public boolean run(String playerName) {
		if (!hasPermission()) {
			return true;
		}

		// check if they can do stuff here
		if (SignUtilities.instance.config.getConfig().getBoolean("safeSigns.newsSign")) {
			if (!Utils.canEdit(playerName, sign.getBlock())) {
				return false;
			}
		}

		String newsText = "";
		String oldNewsText = "";

		// All the signs near each other are here
		ArrayList<Sign> signs = getAllAdjSign(sign, face);

		if (signs.size() == 0) {
			return true;
		}

		for (Sign csign : signs) {
			for (int i = 1; i < 4; i++) {
				if (csign.getLine(i) != "") {
					oldNewsText = oldNewsText + csign.getLine(i) + ";";
				}
			}
		}

		oldNewsText = oldNewsText.substring(0, Math.max(oldNewsText.length() - 1, 0));

		Boolean findNews = true;

		// While the second line of the sign is the same we try to find another news except if there is only one news
		String name = signs.get(0).getLine(0).split(":", 2)[1];
		int newsNb = SignUtilities.instance.newsMgr.getNewsNumber(name);

		while (findNews) {
			newsText = SignUtilities.instance.newsMgr.getNextNews(name);

			if (!newsText.equalsIgnoreCase(oldNewsText) || newsText.split(";", 2)[0].equalsIgnoreCase(ChatColor.RED + "NO NEWS") || newsNb == 1) {
				findNews = false;
			}
		}

		if (SignUtilities.instance.config.autoLineCut) {
			newsText = autoLine(newsText);
		}

		String[] newsLines = newsText.split(";");
		SUEditor suEditor = new SUEditor();

		int i = 0;
		for (Sign csign : signs) {
			for (int j = 0; j < 3; j++) {

				if (newsLines.length > 3 * i + j) {
					suEditor.changeText(csign, newsLines[3 * i + j], j + 1, player);
				} else {
					suEditor.changeText(csign, "", j + 1, player);
				}
			}

			i++;
		}

		return true;
	}

	public ArrayList<Sign> getAllAdjSign(Sign mainSign, BlockFace face) {
		ArrayList<Sign> signs = new ArrayList<Sign>();
		BlockFace left = getLeftBlockFace(face);
		BlockFace right = getRightBlockFace(face);

		boolean running = true;
		Block firstBlock = mainSign.getBlock();

		// check if the sign has the same first line on the block to the left
		if (left != null) {
			while (running) {
				Block leftBlock = firstBlock.getRelative(left);

				if (Utils.isASign(leftBlock)) {
					Sign leftSign = (Sign) leftBlock.getState();

					if (leftSign.getLine(0).equalsIgnoreCase(mainSign.getLine(0))) {
						firstBlock = leftBlock;
					} else {
						running = false;
					}

				} else {
					running = false;
				}
			}
		}

		// check if the sign has the same first line on the block to the left
		if (right != null) {
			running = true;
			Block rightBlock = firstBlock;

			while (running) {

				if (Utils.isASign(rightBlock)) {
					Sign rightSign = (Sign) rightBlock.getState();

					if (rightSign.getLine(0).equalsIgnoreCase(mainSign.getLine(0))) {
						signs.add(rightSign);
					} else {
						running = false;
					}

				} else {
					running = false;
				}

				rightBlock = rightBlock.getRelative(right);
			}
		}

		return signs;
	}

	public BlockFace getLeftBlockFace(BlockFace face) {
		BlockFace left = null;

		if (face == BlockFace.NORTH) {
			left = BlockFace.EAST;
		} else if (face == BlockFace.EAST) {
			left = BlockFace.SOUTH;
		} else if (face == BlockFace.SOUTH) {
			left = BlockFace.WEST;
		} else if (face == BlockFace.WEST) {
			left = BlockFace.NORTH;
		}

		return left;
	}

	public BlockFace getRightBlockFace(BlockFace face) {
		BlockFace right = null;

		if (face == BlockFace.NORTH) {
			right = BlockFace.WEST;
		} else if (face == BlockFace.WEST) {
			right = BlockFace.SOUTH;
		} else if (face == BlockFace.SOUTH) {
			right = BlockFace.EAST;
		} else if (face == BlockFace.EAST) {
			right = BlockFace.NORTH;
		}

		return right;
	}

	public String autoLine(String str) {
		String[] oldLines = str.split(";");
		String newStr = "";

		for (String line : oldLines) {

			while (line.length() > 15) {
				newStr = newStr + line.substring(0, 15) + ";";
				line = line.substring(15, line.length());
			}

			newStr = newStr + line + ";";
		}

		newStr = newStr.substring(0, newStr.length() - 1);
		return newStr;
	}
}
