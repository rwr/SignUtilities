/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.SignCommands;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.Colors;
import com.timcolonel.SignUtilities.SignAction;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;

public class CmdLinkSign extends CmdSign {
	protected SignAction action;
	protected String cmds[] = new String[4];
	int selected = 0;

	public CmdLinkSign(Player player, Sign sign, Block block, SignAction action) {
		super(player, sign, block);
		this.permission = "signutilities.signs.cmd.custom.use";

		this.action = action;
		int i = 0;
		for (String line : sign.getLines()) {
			cmds[i] = "";

			if (line.contains("[") && line.contains("]")) {

				if (Colors.ContainColor(line, ChatColor.DARK_BLUE)) {
					selected = i;
				}

				String str;
				str = Colors.removeColor(line);
				str = str.substring(1, str.length() - 1);

				if (SignUtilities.instance.config.cmdLinks.containsKey(str)) {
					cmds[i] = str;
				}
			}

			i++;
		}

		if (!cmds[selected].equalsIgnoreCase("")) {
			cmdStr = SignUtilities.instance.config.cmdLinks.get(cmds[selected]);
		} else {
			cmdStr = "";
		}
	}

	@Override
	public boolean run(String playerName) {
		if (!hasPermission()) {
			return true;
		}

		// check if they can do stuff here
		if (SignUtilities.instance.config.getConfig().getBoolean("safeSigns.cmdLinkSign")) {
			if (!Utils.canEdit(playerName, sign.getBlock())) {
				return false;
			}
		}

		if (!cmdStr.equalsIgnoreCase("")) {

			// If left click on sign, execute
			if (action == SignAction.LeftClick) {
				executeCommand();

			} else if (action == SignAction.RightClick) {
				// If right click change position
				int prevSel = selected;
				selected++;
				selected %= 4;

				while (cmds[selected].equalsIgnoreCase("")) {
					selected++;
					selected %= 4;
				}

				// Remove the color on the previous selection
				sign.setLine(prevSel, Colors.removeColor(sign.getLine(prevSel)));

				// set the color to the new selection
				sign.setLine(selected, ChatColor.DARK_BLUE + sign.getLine(selected));
				sign.update();

				cmdStr = SignUtilities.instance.config.cmdLinks.get(cmds[selected]);
			}

			return true;

		} else {
			return false;
		}
	}
}
