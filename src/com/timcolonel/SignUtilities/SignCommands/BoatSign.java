/**
 * SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 * Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.timcolonel.SignUtilities.SignCommands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SUBoat;
import com.timcolonel.SignUtilities.SignUtilities;
import com.timcolonel.SignUtilities.Utils;
import com.timcolonel.SignUtilities.Vector;
import com.timcolonel.SignUtilities.Response.TransactionResponse;
import java.util.Arrays;
import java.util.HashSet;

public class BoatSign extends SignCommand {

	public BoatSign(Player player, Sign sign, Block block) {
		super(player, sign, block);
		this.permission = "signutilities.signs.boat.use";
	}

	@Override
	public boolean run(String playerN) {
		if (!hasPermission()) {
			return true;
		}

		// Create the block where the boat will spawn or be removed, by default the sign
		Block positionBlock = sign.getBlock();

		// check if they can do stuff here
		if (SignUtilities.instance.config.getConfig().getBoolean("safeSigns.boatSign")) {
			if (!Utils.canEdit(playerN, positionBlock)) {
				return false;
			}
		}

		String playerName = sign.getLine(1);
		double cost = 0;
		try {
			cost = Integer.parseInt(sign.getLine(2));
		} catch (Exception e) {
		}

		Vector relative = new Vector();

		if (sign.getLine(3).isEmpty()) {
			// default boat position if last line of sign is blank

			// find water around sign
			Block signBlock = sign.getBlock();
			HashSet<Integer> waterIds = new HashSet<Integer>(Arrays.asList(8, 9));
			Block waterBlock = null;

			HashSet<Location> locations = new HashSet<Location>();
			locations.add(signBlock.getRelative(1, 0, 0).getLocation());
			locations.add(signBlock.getRelative(0, 0, 1).getLocation());
			locations.add(signBlock.getRelative(-1, 0, 0).getLocation());
			locations.add(signBlock.getRelative(0, 0, -1).getLocation());

			for (Location l : locations) {
				waterBlock = Utils.closestBlock(l, waterIds);
				if (waterBlock != null) {
					relative.x = waterBlock.getLocation().getBlockX();
					relative.y = waterBlock.getLocation().getBlockY();
					relative.z = waterBlock.getLocation().getBlockZ();
					break;
				}
			}
			
			if (waterBlock == null) {
				relative.extractVector("0;0;0");
			}

		} else {
			// get cart position from bottom line of the sign
			relative.extractVector(sign.getLine(3));
		}
/*
		int X = positionBlock.getX();
		int Y = positionBlock.getY();
		int Z = positionBlock.getZ();
*/
		Location boatLocation = new Location(player.getWorld(), 0.5 + relative.x, 0.5 + relative.y, 0.5 + relative.z);
		Block chestBlock = sign.getBlock().getRelative(0, -1, 0);
		// Load the boat at the spawnPostition to see if there is one
		SUBoat b = SignUtilities.instance.boatMgr.isBoatAt(boatLocation);
		// If there is no Boat, continue the process of creating a boat
		if (b == null) {

			// Check if there is a chest containing the boats
			if (chestBlock.getType() == Material.CHEST) {

				// Check if there are some boats remaining in the chest
				if (((Chest) chestBlock.getState()).getInventory().contains(Material.BOAT.getId())) {

					boolean spawnBoat = false;
					if (cost > 0) {

						// Make the player pay the price of use.
						TransactionResponse response = SignUtilities.instance.pluginsMgr.ecoPay(player, playerName, cost);

						if (response.success()) {
							spawnBoat = true;
						} else {
							player.sendMessage(ChatColor.RED + response.getErrorMessage());
						}
					} else {
						spawnBoat = true;
					}

					if (spawnBoat) {
						// Spawn the boat
						Boat boat = player.getWorld().spawn(boatLocation, Boat.class);

						// Create the signUtilities BoatObject that will allow to the Boat to go back to is initial chest when destroyed
						SUBoat su_boat = new SUBoat(boat, (Chest) chestBlock.getState(), SignUtilities.instance);
						SignUtilities.instance.boatMgr.addBoat(boat, su_boat);
					}

				} else {
					player.sendMessage(ChatColor.RED + "No Boat in chest");
				}

			} else {
				player.sendMessage(ChatColor.RED + "No chest");
			}

		} else {
			SignUtilities.instance.boatMgr.boatDestroyed(b.getBoat());
		}

		return true;
	}
}