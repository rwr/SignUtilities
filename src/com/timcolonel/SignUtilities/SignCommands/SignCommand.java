/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.SignCommands;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.timcolonel.SignUtilities.SignUtilities;

public class SignCommand {
	protected Sign sign;
	protected Player player;

	// Contains the block from where the sign has been activated (Sign itself or a button for example)
	protected Block selectedBlock;

	protected String permission;

	public SignCommand(Player player, Sign sign, Block block) {
		this.player = player;
		this.sign = sign;
		this.selectedBlock = block;
		this.permission = "signutilities.*";
	}

	public boolean run(String playerName) {
		return true;
	}

	public boolean hasPermission() {
		if (SignUtilities.instance.pluginsMgr.hasPermission(player, permission)) {
			return true;
		} else {
			player.sendMessage(ChatColor.RED + "You can't use that sign");
			return false;
		}
	}

	/********************************************************************************************
	 * SETTER - GETTERS *
	 ********************************************************************************************/
	public void setPlayer(Player player) {
		this.player = player;

	}

	public void setSign(Sign sign) {
		this.sign = sign;
	}

	public Player getPlayer() {
		return this.player;
	}

	public Sign getSign() {
		return this.sign;
	}

	public Block getSelectedBlock() {
		return this.selectedBlock;
	}

	public void setSelectedBlock(Block selectedBlock) {
		this.selectedBlock = selectedBlock;
	}
}
