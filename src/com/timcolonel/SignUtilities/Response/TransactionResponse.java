/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Response;

public class TransactionResponse extends Response {
	double amount;

	// ////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS //
	// ////////////////////////////////////////////////////////////////////

	public TransactionResponse() {
		this(true, "", 0);
	}

	public TransactionResponse(boolean success) {
		this(success, "", 0);
	}

	public TransactionResponse(boolean success, String message) {
		this(true, message, 0);
	}

	public TransactionResponse(boolean success, double amount) {
		this(true, "", amount);
	}

	public TransactionResponse(boolean success, String message, double amount) {
		this.success = success;
		this.message = message;
		this.amount = amount;
	}

	// ////////////////////////////////////////////////////////////////////
	// GETTERS //
	// ////////////////////////////////////////////////////////////////////

	public double getAmountTransfered() {
		return amount;
	}
}
