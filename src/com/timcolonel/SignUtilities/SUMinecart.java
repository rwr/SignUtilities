/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities;

import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Minecart;
import org.bukkit.inventory.ItemStack;

public class SUMinecart {

	private Minecart minecart;
	Chest nativeChest;

	public SUMinecart() {
		setMinecart(null);
		nativeChest = null;
	}

	public SUMinecart(Minecart m) {
		setMinecart(m);
		nativeChest = null;
	}

	public SUMinecart(Minecart m, Chest c, SignUtilities instance) {
		setMinecart(m);
		nativeChest = c;
		removeMinecartFromChest();
	}

	public void minecartDestroyed(Boolean b) {
		if (nativeChest != null) {
			nativeChest.getInventory().addItem(new ItemStack(Material.MINECART.getId(), 1));
		}
	}

	public boolean removeMinecartFromChest() {

		if (nativeChest != null) {

			// If the chest contain at least one minecart we remove one
			if (nativeChest.getInventory().contains(Material.MINECART.getId())) {
				int slot = nativeChest.getInventory().first(Material.MINECART.getId());
				ItemStack itStack = nativeChest.getInventory().getItem(slot);

				if (itStack.getAmount() > 1) {
					itStack.setAmount(itStack.getAmount() - 1);
					nativeChest.getInventory().setItem(slot, itStack);

				} else {
					nativeChest.getInventory().clear(slot);
				}

				// nativeChest.getInventory().remove(Material.CHEST.getId());
				return true;
			} else {
				return false;
			}

		} else
			return false;
	}

	/********************************************************************************************
	 * SETTER - GETTERS *
	 ********************************************************************************************/
	public Chest getNativeChest() {
		return nativeChest;
	}

	public void setNativeChest(Chest nativeChest) {
		this.nativeChest = nativeChest;
	}

	public Minecart getMinecart() {
		return minecart;
	}

	public void setMinecart(Minecart minecart) {
		this.minecart = minecart;
	}

}
