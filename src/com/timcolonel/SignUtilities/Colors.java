/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities;

import org.bukkit.ChatColor;

public class Colors {

	public static String findColor(String str) {
		// if line only has 1 character return it
		if (str.length() == 1) {
			return str;
		}

		String[] splitLine = str.split("&");
		String newLine = splitLine[0];

		for (int j = 1; j < splitLine.length; j++) {
			int col;
			col = "0123456789abcdef".indexOf(splitLine[j].toLowerCase().charAt(0));

			// Double ## ou &&
			// If the first character is not one of those 0123456789abcdef
			if (splitLine[j].length() == 0 || col == -1) {
				newLine += "&";

			} else {
				// Otherwise replace the & with the § characters that will be automatically translate to chatcolor
				newLine += "§";
			}
			newLine += splitLine[j];
		}

		return newLine;

	}

	// Translate the color format of bukkit to the '&' format
	public static String decodeColor(String str) {

		String newStr = "";
		for (int i = 0; i < str.length(); i++) {

			if (str.charAt(i) == 167) {
				newStr += "&";
			} else {
				newStr += str.charAt(i);
			}

		}

		return newStr;

	}

	// Remove all the color to get the text
	public static String removeColor(String str) {

		String newStr = "";
		int i = 0;
		while (i < str.length()) {

			if (str.charAt(i) == 167) {

				// Skip the next char as it's the color code.
				i++;

			} else {
				newStr += str.charAt(i);
			}
			i++;
		}

		return newStr;

	}

	public static boolean ContainColor(String str, ChatColor color) {
		return str.contains("" + color);
	}
}
