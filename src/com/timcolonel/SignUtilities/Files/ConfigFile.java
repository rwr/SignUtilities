/**
 *   SignUtilities - Utilities for signs like edit, colors, and copy and paste.
 *   Copyright (C) 2012 Ryan Rhode - rrhode@gmail.com
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.timcolonel.SignUtilities.Files;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.timcolonel.SignUtilities.SignUtilities;

public class ConfigFile {

	protected FileConfiguration config;
	protected File configFile;
	protected String name;

	public ConfigFile(String fileName) {
		name = fileName;
	}

	private void makeFolder() {
		if (SignUtilities.instance == null) {
			System.out.println("instance is null");
		}
		if (SignUtilities.instance.getDataFolder() == null) {
			System.out.println("instance is null");
		}
		SignUtilities.instance.getDataFolder().mkdir();
	}

	public void saveDefaultConfig() {
		if (configFile == null) {
			makeFolder();
			configFile = new File(SignUtilities.instance.getDataFolder(), name);
		}
		if (!configFile.exists()) {
			SignUtilities.instance.saveResource(name, false);
		}
	}

	public void reloadConfig() {
		if (configFile == null) {
			makeFolder();
			configFile = new File(SignUtilities.instance.getDataFolder(), name);
		}
		config = YamlConfiguration.loadConfiguration(configFile);

		// load default config if it's there
		InputStream defaultStream = SignUtilities.instance.getResource(name);
		if (defaultStream != null) {
			YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(defaultStream);
			config.setDefaults(defaultConfig);
		}
	}

	public FileConfiguration getConfig() {
		if (config == null) {
			reloadConfig();
		}
		return config;
	}

	public void saveConfig() {
		if (config == null || configFile == null) {
			return;
		}
		try {
			getConfig().save(configFile);
		} catch (IOException ex) {
			SignUtilities.instance.logger.severe("Could not save file to " + configFile);
		}
	}
}