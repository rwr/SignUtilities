package com.timcolonel.SignUtilities.Files;

import java.util.HashMap;
import java.util.Set;

import com.timcolonel.SignUtilities.SignUtilities;

public class MainConfig extends ConfigFile {
	// Variables which need to be load
	public int dispMsg;
	public int itemSel;
	public boolean autoLineCut;
	public boolean keepSignCopy = false;
	public final String[] cmdSignColor = new String[4];
	public final HashMap<String, String> cmdLinks = new HashMap<String, String>();
	public final HashMap<String, String> webLinks = new HashMap<String, String>();

	public MainConfig(String fileName) {
		super(fileName);
	}

	/**
	 * Load config.yml values
	 */
	public void loadConfig() {
		saveDefaultConfig();
		reloadConfig();

		dispMsg = getConfig().getInt("DisplayMsg", 0);
		autoLineCut = getConfig().getBoolean("autoLineCut", false);
		keepSignCopy = getConfig().getBoolean("keepSignCopy", false);
		itemSel = getConfig().getInt("itemForSelection", 280);
		cmdSignColor[0] = getConfig().getString("cmdSign.line1", "");
		cmdSignColor[1] = getConfig().getString("cmdSign.line2", "");
		cmdSignColor[2] = getConfig().getString("cmdSign.line3", "");
		cmdSignColor[3] = getConfig().getString("cmdSign.line4", "");

		if (config.isConfigurationSection("cmdLinks")) {
			Set<String> keys = getConfig().getConfigurationSection("cmdLinks").getKeys(false);

			if (keys != null) {
				for (String key : keys) {
					String cmdStr = getConfig().getString("cmdLinks." + key, "");
					cmdLinks.put(key, cmdStr);
				}
			}

		} else {
			SignUtilities.instance.logger.info("No cmdLinks section in the config file, creating it.");
			getConfig().createSection("cmdLinks");
		}

		if (getConfig().isConfigurationSection("webLinks")) {
			Set<String> keys = getConfig().getConfigurationSection("webLinks").getKeys(false);

			if (keys != null) {
				for (String key : keys) {
					String cmdStr = getConfig().getString("webLinks." + key, "");
					webLinks.put(key, cmdStr);
				}
			}

		} else {
			SignUtilities.instance.logger.info("No cmd Links Directory in the config file, creating it.");
			getConfig().createSection("webLinks");
		}

		SignUtilities.instance.saveConfig();
	}

}
